from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from .forms import InicioSesionForma, RegistroForma


def seleccionar(request):
    return render(request, "parras_inc_auth/seleccionar.html", {})


def iniciar_sesion(request):
    message = None
    form = InicioSesionForma(None)

    if request.method == "POST":
        form = InicioSesionForma(request.POST or None)
        if form.is_valid():
            nombre_usuario = request.POST.get("username")
            clave = request.POST.get("password")
            usuario = authenticate(request, username=nombre_usuario, password=clave)
            if usuario is not None:
                if usuario.is_active:
                    login(request, usuario)
                    return redirect(reverse_lazy("seleccionar"))
                else:
                    message = "Usuario está inactivo"
            else:
                message = "Usuario o contraseña incorrecta"
        else:
            message = "Forma inválida"

    context = {"form": form, "message": message}
    return render(request, "parras_inc_auth/inicio_sesion.html", context)


def cerrar_sesion(request):
    logout(request)
    return redirect(reverse_lazy("seleccionar"))


def registro(request):
    message = None
    form = RegistroForma(None)

    if request.method == "POST":
        form = RegistroForma(request.POST or None)
        if form.is_valid():
            correo = request.POST.get("email")
            clave = request.POST.get("password")
            confirmar_clave = request.POST.get("confirm_password")
            if clave == confirmar_clave:
                usuario = User.objects.create_user(username=correo)
                usuario.set_password(clave)
                usuario.save()
                login(request, usuario)
                return redirect(reverse_lazy("seleccionar"))
            else:
                message = "Las contraseñas no concuerdan"
        else:
            message = "Forma inválida"

    context = {"form": form, "message": message}
    return render(request, "parras_inc_auth/registro.html", context)

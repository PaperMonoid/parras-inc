from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from .models import Investigador, Investigacion
from .forms import InvestigadorForma, InvestigacionForma


def investigador_nuevo(request):
    message = None
    form = InvestigadorForma(request.POST or None)

    if request.user.is_authenticated:
        investigador = Investigador.objects.filter(usuario=request.user).first()
        if investigador is None:
            if request.method == "POST":
                if form.is_valid():
                    form.instance.usuario = request.user
                    form.save()
                    return redirect(reverse_lazy("investigacion_lista"))
                else:
                    message = "Forma inválida"
            contexto = {"form": form, "message": message}
            return render(request, "investigacion/investigador_nuevo.html", contexto)
        else:
            return redirect(reverse_lazy("investigacion_lista"))

    return redirect(reverse_lazy("iniciar_sesion"))


def investigacion_lista(request, categoria=None):
    if request.user.is_authenticated:
        investigador = Investigador.objects.filter(usuario=request.user).first()
        if investigador:
            if categoria is None:
                investigaciones = Investigacion.objects.all()
            else:
                investigaciones = Investigacion.objects.filter(categoria=categoria)
            contexto = {"investigaciones": investigaciones}
            return render(request, "investigacion/investigacion_lista.html", contexto)
        else:
            return redirect(reverse_lazy("investigador_nuevo"))

    return redirect(reverse_lazy("iniciar_sesion"))


def investigacion_nuevo(request):
    message = None
    form = InvestigacionForma(request.POST or None, request.FILES or None)

    if request.user.is_authenticated:
        investigador = Investigador.objects.filter(usuario=request.user).first()
        if investigador:
            if request.method == "POST":
                if form.is_valid():
                    form.instance.investigador = investigador
                    form.save()
                    return redirect(reverse_lazy("investigacion_lista"))
                else:
                    message = "Forma inválida"
            contexto = {"form": form, "message": message}
            return render(request, "investigacion/investigacion_nuevo.html", contexto)
        else:
            return redirect(reverse_lazy("investigador_nuevo"))

    return redirect(reverse_lazy("iniciar_sesion"))

from django import forms


class InicioSesionForma(forms.Form):
    username = forms.CharField(max_length=128, widget=forms.TextInput())
    password = forms.CharField(max_length=32, widget=forms.PasswordInput())


class RegistroForma(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput())
    password = forms.CharField(max_length=128, widget=forms.PasswordInput())
    confirm_password = forms.CharField(max_length=32, widget=forms.PasswordInput())

from django.urls import reverse_lazy
from django.shortcuts import render, redirect

from .models import Software
from .forms import SoftwareForma, DepartamentoForma


def departamento_nuevo(request):
    message = None
    form = DepartamentoForma(request.POST or None)

    if request.user.is_authenticated:
        if request.method == "POST":
            if form.is_valid():
                form.instance.usuario = request.user
                form.save()
                return redirect(reverse_lazy("software_lista"))
            else:
                message = "Forma inválida"
        contexto = {"form": form, "message": message}
        return render(request, "software/departamento_nuevo.html", contexto)

    return redirect(reverse_lazy("iniciar_sesion"))


def software_lista(request):
    if request.user.is_authenticated:
        tipo = request.GET.get("tipo")
        buscar = request.GET.get("buscar") or ""
        if buscar is not "" and tipo == "nombre":
            software_lista = Software.objects.filter(nombre=buscar)
        elif buscar is not "" and tipo == "funcion":
            software_lista = Software.objects.filter(funcion=buscar)
        elif buscar is not "" and tipo == "departamento":
            software_lista = Software.objects.filter(departamento__nombre=buscar)
        else:
            software_lista = Software.objects.all()
        contexto = {"software_lista": software_lista}
        return render(request, "software/software_lista.html", contexto)

    return redirect(reverse_lazy("iniciar_sesion"))


def software_nuevo(request):
    message = None
    form = SoftwareForma(request.POST or None)

    if request.user.is_authenticated:
        if request.method == "POST":
            if form.is_valid():
                form.instance.usuario = request.user
                form.save()
                return redirect(reverse_lazy("software_lista"))
            else:
                message = "Forma inválida"
        contexto = {"form": form, "message": message}
        return render(request, "software/departamento_nuevo.html", contexto)

    return redirect(reverse_lazy("iniciar_sesion"))

from django.forms import ModelForm
from .models import Investigador, Investigacion


class InvestigadorForma(ModelForm):
    class Meta:
        model = Investigador
        fields = ["titulo", "nombre", "apellidos"]


class InvestigacionForma(ModelForm):
    class Meta:
        model = Investigacion
        fields = ["titulo", "resumen", "categoria", "archivo"]

# Generated by Django 2.2.6 on 2019-10-19 05:26

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Departamento',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Software',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=64)),
                ('funcion', models.CharField(max_length=64)),
                ('departamento', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='software.Departamento')),
            ],
        ),
    ]

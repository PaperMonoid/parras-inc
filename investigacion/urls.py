from django.urls import path, include
from . import views

urlpatterns = [
    path("investigador_nuevo", views.investigador_nuevo, name="investigador_nuevo"),
    path("lista", views.investigacion_lista, name="investigacion_lista"),
    path(
        "lista/<str:categoria>",
        views.investigacion_lista,
        name="investigacion_lista_categoria",
    ),
    path("nuevo", views.investigacion_nuevo, name="investigacion_nuevo"),
]

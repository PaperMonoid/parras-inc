import uuid
from django.db import models
from django.contrib.auth.models import User


class Investigador(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    titulo = models.CharField(max_length=128)
    nombre = models.CharField(max_length=64)
    apellidos = models.CharField(max_length=64)


class Investigacion(models.Model):
    doi = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    investigador = models.ForeignKey(Investigador, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=256)
    resumen = models.CharField(max_length=512)
    CATEGORIAS = [
        ("Ciencias Computacionales", "Ciencias Computacionales"),
        ("Ciencias de la Tierra", "Ciencias de la Tierra"),
        ("Ciencias Naturales", "Ciencias Naturales"),
        ("Ciencias Sociales", "Ciencias Sociales"),
        ("Ciencias Medicas", "Ciencias Medicas"),
    ]
    categoria = models.CharField(
        max_length=64, choices=CATEGORIAS, default="Ciencias Computacionales"
    )
    archivo = models.FileField(upload_to="archivos/")

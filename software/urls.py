from django.urls import path, include
from . import views

urlpatterns = [
    path("departamento_nuevo", views.departamento_nuevo, name="departamento_nuevo"),
    path("lista", views.software_lista, name="software_lista"),
    path("nuevo", views.software_nuevo, name="software_nuevo"),
]

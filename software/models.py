import uuid
from django.db import models


class Departamento(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField(max_length=64)

    def __str__(self):
        return self.nombre


class Software(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField(max_length=64)
    funcion = models.CharField(max_length=64)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)

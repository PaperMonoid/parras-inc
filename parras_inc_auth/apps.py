from django.apps import AppConfig


class ParrasIncAuthConfig(AppConfig):
    name = 'parras_inc_auth'

from django.forms import ModelForm
from .models import Departamento, Software


class DepartamentoForma(ModelForm):
    class Meta:
        model = Departamento
        fields = ["nombre"]


class SoftwareForma(ModelForm):
    class Meta:
        model = Software
        fields = ["nombre", "funcion", "departamento"]
